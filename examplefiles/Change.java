// Oops, public class name not the same as file name
public class Change {

    public void changeInside(final String CurlyBraces){
        final String changeAWord = "There is a word wrong in this sentence"; // Enter here
    }

    public void changeInside(final String CurlyBraces) {
        final List<String> list = Arrays.asList("VIM", "Awesome", "Why would you use anything different?");
        list.stream()
            .filter(word -> word.contains(" "))
            .forEach(word -> System.out.println(word.toUpperCase()));
    }

    public void changeInside(final String CurlyBraces){
        final List<String> list = Arrays.asList("");
        System.out.println("word");
        System.out.println("word");
        System.out.print("word");
        System.out.println("word");
        System.out.println("word");
        System.out.println("word");


        System.out.println("word");
        System.out.println("word");
        System.out.println("word");
        System.out.println("word");
        System.out.println("word");
    }

    public void changeInside(final String CurlyBraces) {
        final List<String> list = Arrays.asList("VIM", "Awesome", "Why would you use anything different?");
        list.stream()
            .filter(word -> word.contains(" "))
            .forEach(word -> System.out.println(word.toUpperCase()));
    }


}
