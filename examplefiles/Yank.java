public class Yank {

   public void yankAParagraph(final String CurlyBraces) {
      final List<String> list = Arrays.asList("VIM", "Awesome", "Why would you use anything different?");
      list.stream()
              .filter(word -> word.contains(" "))
              .forEach(word -> System.out.println(word.toUpperCase()));
   }

   public void yankAParagraphWhiteLine(final String CurlyBraces) {
      final String whatIsAParagraphInVim = "All lines between a whiteline"
      final List<String> list = Arrays.asList("VIM", "Awesome", "Why would you use anything different?");
      list.stream()

              .filter(word -> word.contains(" "))
              .forEach(word -> System.out.println(word.toUpperCase()));
   }

   public void yankInside(final String CurlyBraces) {
      final String whatDidYouSay = "Yank everything inside";
      final List<String> list = Arrays.asList("VIM", "Awesome", "Why would you use anything different?");

      list.stream()
              .filter(word -> word.contains(" "))
              .forEach(word -> System.out.println(word.toUpperCase()));
   }

   public void visualYank(final String CurlyBraces) {
      final String whatDidYouSay = "Visual yanking";
      final List<String> list = Arrays.asList("VIM", "Awesome", "Why would you use anything different?");
      list.stream()

              .filter(word -> word.contains(" "))
              .forEach(word -> System.out.println(word.toUpperCase()));
   }
}
