PRESENTATION_ADOC_FILE=VimPresentation.adoc
OUTPUT_PDF_FILE=handout.pdf
OUTPUT_EPUB_FILE=handout.epub
OUTPUT_HTML_FILE=handout.html
OUTPUT_SLIDES=index.html
OPT=
# Path to deck2pdf 
DECKPDF=/Users/jeroenwerk/deck2pdf-0.3.0/bin/deck2pdf

# What to do, when make is called without parameters
all: clean pdf slides present

# remove the output files
clean:
	rm -f $(OUTPUT_PDF_FILE) $(OUTPUT_EPUB_FILE) $(OUTPUT_HTML_FILE) $(OUTPUT_SLIDES) $(DECKOUT)

# generate the slides of the talk
slides:
	bundle exec asciidoctor-revealjs -o $(OUTPUT_SLIDES) $(PRESENTATION_ADOC_FILE)

# generate Handouts in various formats
pdf:
	asciidoctor-pdf -b pdf -o $(OUTPUT_PDF_FILE) $(PRESENTATION_ADOC_FILE)

epub:
	asciidoctor-epub3 -b epub3 -o $(OUTPUT_EPUB_FILE) $(PRESENTATION_ADOC_FILE)

html:
	asciidoctor -b html5 -o $(OUTPUT_HTML_FILE) $(PRESENTATION_ADOC_FILE)

present:
	ruby -run -e httpd . -p 5000 127.0.0.1

